package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.AsyncService;
import com.example.demo.service.MainService;
import com.example.demo.service.ThreadService;
import com.example.demo.utility.MyThreads;
import com.example.demo.utility.MyextendedThread;

@RestController
public class MainAController {

	@Autowired
	MainService serv;
	
	@Autowired
	ThreadService threadService;
	
	@Autowired
	AsyncService asyncService;
	
	/*
	 * @Autowired MyThreads myThreads ;
	 */
	Logger logger = LoggerFactory.getLogger(MainAController.class);
	
	//MyextendedThread myextendedThread;
	
	@GetMapping("/extendedThread")
	public String extendedThreads() {
		System.out.println("Running threAD");
		logger.info("Running threAD");
		//serv.asyncMethod();
		
		/*
		 * Thread thread1 = new Thread(serv); thread1.start();
		 */
		
		
		threadService.start();
		/*
		 * myextendedThread = new MyextendedThread(); myextendedThread.start();
		 */
		
		System.out.println("After threds call in controller");
		logger.info("After threds call in controller");
		

		
		return "Testing thead with extended Thread class...";
	}
	
	
	@GetMapping("/runnableThread")
	public String runnableThreads() {
		System.out.println("Running threAD");
		logger.info("Running threAD");
		
		//calling thread implemented with runnable
		 Thread thread1 = new Thread(serv);
		 thread1.start();

		
		System.out.println("After threds call in controller");
		logger.info("After threds call in controller");
		

		
		return "Testing thead implemented with runnable...";
	}
	
	@GetMapping("/asyncThread")
	public String asyncThreads() {
		System.out.println("Running threAD");
		logger.info("Running threAD");
	
		
		//calling thread annotaded with @Async
		asyncService.asyncMethod();
		
		System.out.println("After threds call in controller");
		logger.info("After threds call in controller");
		

		
		return "Testing thead with Async annoration...";
	}
	

}
