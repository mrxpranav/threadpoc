package com.example.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.demo.utility.MyThreads;

@Service
public class MainService implements Runnable {
	


	@Autowired
	MyThreads threads;
	
	Logger logger = LoggerFactory.getLogger(MainService.class);
	@Override
	public void run() {
		System.out.println("Thread1 running"); 
		logger.info("Thread1 running"); 
		
		try {
			System.out.println("Going to thread2");
			logger.info("Going to thread2");
			Thread thrd1 = new Thread(threads);
			thrd1.start();
			Thread.sleep(5000);
			
			 } catch (InterruptedException e)
		{ 
				 e.printStackTrace();
		}
		System.out.println("Thread1 Ends");
		logger.info("Thread1 Ends");
				 }

}
