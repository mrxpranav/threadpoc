package com.example.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.demo.utility.MyextendedThread;

@Service
public class ThreadService extends Thread {

	@Autowired
	MyextendedThread thread;
	
	Logger logger = LoggerFactory.getLogger(ThreadService.class);

	@Override
	public void run() {

		System.out.println("Thread1 running"); 
		logger.info("Thread1 running"); 
		try {
			System.out.println("Going to thread2");
			logger.info("Going to thread2");

			thread.start();
			Thread.sleep(5000);

			
			 } catch (InterruptedException e)
		{ 
				 e.printStackTrace();
		}
		System.out.println("Thread1 Ends");
		logger.info("Thread1 Ends");
	}

}
