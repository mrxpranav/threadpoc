package com.example.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.demo.utility.AsyncThread2;

@Service
public class AsyncService {
	
	@Autowired
	AsyncThread2 asyncThread2;

	Logger logger = LoggerFactory.getLogger(AsyncService.class);

	@Async
	public void asyncMethod() {
		System.out.println("Async thread1 Running");
		logger.info("Async thread1 Running");
		try {
			System.out.println("Going to Async thread2 ");
			logger.info("Going to Async thread2 ");
			asyncThread2.runAsyncNested();
			Thread.sleep(5000);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Async thread1 Ends");
		logger.info("Async thread1 Ends");
	}

}
