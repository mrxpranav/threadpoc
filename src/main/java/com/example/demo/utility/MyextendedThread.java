package com.example.demo.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MyextendedThread extends Thread {
	
	Logger logger = LoggerFactory.getLogger(MyextendedThread.class);
	@Override
	public void run() {
		System.out.println("Thread2 is running");
		logger.info("Thread2 is running");
		try {
			//service.asyncMethod();
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Thread2 Ends");
		logger.info("Thread2 Ends");
		
	}

}
