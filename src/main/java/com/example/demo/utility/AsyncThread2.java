package com.example.demo.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AsyncThread2 {
	
	Logger logger = LoggerFactory.getLogger(AsyncThread2.class);
	
	@Async
	public void runAsyncNested() {
		System.out.println("Thread2 is running");
		logger.info("Thread2 is running");
		try {
			
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		System.out.println("Thread2 Ends");
		logger.info("Thread2 Ends");
		
	}

}
